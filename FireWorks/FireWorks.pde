ArrayList<Morter> morters;

void setup()
{
  size(600, 600);
  background(0);
  morters = new ArrayList<Morter>();
  for(int i = 0; i < 15; i++)
  {
    morters.add(new Morter());
  }
}

void draw()
{
  noStroke();
  fill(0, 5);
  rect(0, 0, width, height);

  for(int i = morters.size() - 1; i >= 0; i--)
  {
    Morter[] children = morters.get(i).update();
    if(children == null)
    {
      morters.get(i).render();
    }
    else
    {
      morters.remove(i);
      for(int j = 0; j < children.length; j++)
      {
        morters.add(children[j]);
      }
    }
  }
}
