class Morter
{
  PVector position, lastPosition, velocity, gravity;
  color _color;
  float speed;
  boolean isChild;

  Morter()
  {
    speed = random(1, 3.5);
    position = new PVector(random(20, width - 20), height);
    lastPosition = position.copy();
    velocity = new PVector(random(-0.5, 0.5), -speed);
    gravity = new PVector(0, -0.01);
    _color = color(random(128, 255), random(128, 255), random(128, 255));
    isChild = false;
  }

  Morter(PVector position, PVector velocity, color hue)
  {
    this.position = position.copy();
    lastPosition = position.copy();
    this.velocity = velocity;
    gravity = new PVector(0, -0.01);
    _color = hue;
    isChild = true;
  }

  Morter[] update()
  {
    velocity = velocity.sub(gravity);
    lastPosition = position.copy();
    position = position.add(velocity);

    if(velocity.mag() < 0.4 && !isChild)
    {
      Morter[] children = new Morter[10];
      float angle = 0;
      color hue = color(random(128, 255), random(128, 255), random(128, 255));
      for(int i = 0; i < children.length; i++)
      {
        children[i] = new Morter(position, PVector.fromAngle(angle), hue);
        angle += TWO_PI / children.length;
      }

      return children;
    }
    else
    {
      return null;
    }
  }

  void render()
  {
    pushMatrix();

    stroke(_color);
    strokeWeight(2);
    line(lastPosition.x, lastPosition.y, position.x, position.y);

    popMatrix();
  }
}
