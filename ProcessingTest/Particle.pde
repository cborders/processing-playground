class Particle
{
  PVector position, previousPosition, velocity, acceleration;
  float maxSpeed = 5;

  Particle()
  {
    position = new PVector(random(width - 1), random(height - 1));
    previousPosition = position.copy();
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
  }

  void update(PVector[] flowField)
  {
    // Wrap around
    if(position.x >= width) position.x = 1;
    if(position.x <= 0) position.x = width - 1;
    if(position.y >= height) position.y = 1;
    if(position.y <= 0) position.y = height - 1;

    // Find force
    int x = floor(position.x / scale);
    int y = floor(position.y / scale);
    int index = x + y * columns;

    acceleration.add(flowField[index]);
    velocity.add(acceleration);
    velocity.limit(maxSpeed);

    acceleration.mult(0.2);

    previousPosition = position.copy();
    position = position.add(velocity);
  }

  void render()
  {
    pushMatrix();
    stroke(0, 5);
    strokeWeight(1);
    line(previousPosition.x, previousPosition.y, position.x, position.y);
    popMatrix();
  }
}
