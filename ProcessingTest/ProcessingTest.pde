Particle[] particles;
PVector[] flowField;

int columns, rows, scale;
float increment = 0.002;
float zOff = 0.0;

void setup() {
  size(1200, 800, P2D);
  background(255);

  noiseDetail(8, 0.6);

  scale = 10;
  columns = floor(width / scale);
  rows = floor(height / scale);

  flowField = new PVector[columns * rows];
  particles = new Particle[10000];

  for(int i = 0; i < particles.length; i++)
  {
    particles[i] = new Particle();
  }
}

void draw()
{
  float yOff = 0;
  for(int y = 0; y < rows; y++)
  {
    float xOff = 0;
    for(int x = 0; x < columns; x++)
    {
      int index = x + y * columns;
      float angle = noise(xOff, yOff, zOff) * TWO_PI * 4;
      flowField[index] = PVector.fromAngle(angle);
      flowField[index].setMag(1);

      xOff += increment;
    }

    yOff += increment;
  }

  zOff += increment;

  for(int i = 0; i < particles.length; i++)
  {
    particles[i].update(flowField);
    particles[i].render();
  }
}

void keyPressed()
{
  if(key == ' ')
  {
    saveFrame("flow-" + millis() + ".png");
  }
}
