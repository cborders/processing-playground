class SpaceShip
{
	PVector position;
	PVector velocity;
	PVector acceleration;
	float drag;
	float angle;
	float maxSpeed;
	float rotationSpeed;

	boolean isBoosting;
	boolean turningLeft;
	boolean turningRight;

	SpaceShip(int startX, int startY)
	{
		position = new PVector(startX, startY);
		velocity = new PVector();
		acceleration = new PVector();
		drag = 0.99;
		angle = -PI/2;
		maxSpeed = 5;
		rotationSpeed = 0.05;

		isBoosting = false;
		turningLeft = false;
		turningRight = false;
	}

	void update()
	{
		if(isBoosting)
		{
			acceleration.add(PVector.fromAngle(angle));
		}

		if(turningLeft)
		{
			angle -= rotationSpeed;
		}

		if(turningRight)
		{
			angle += rotationSpeed;
		}

		velocity.add(acceleration);

		if(velocity.mag() > maxSpeed)
		{
			velocity.setMag(maxSpeed);
		}

		position.add(velocity);

		if(position.x > width)
		{
			position.x -= width;
		}
		if(position.x < 0)
		{
			position.x += width;
		}
		if(position.y > height)
		{
			position.y -= height;
		}
		if(position.y < 0)
		{
			position.y += height;
		}

		acceleration.mult(0);
		velocity.mult(drag);
	}

	void rotateLeft(boolean turning)
	{
		turningLeft = turning;
	}

	void rotateRight(boolean turning)
	{
		turningRight = turning;
	}

	void boost(boolean boost)
	{
		isBoosting = boost;
	}

	void draw()
	{
		pushMatrix();
		noFill();
		stroke(255);
		strokeWeight(2);
		translate(position.x, position.y);
		rotate(angle);
		beginShape(TRIANGLES);
		vertex(-10, 5);
		vertex(10, 0);
		vertex(-10, -5);
		endShape();
		popMatrix();
	}
}
