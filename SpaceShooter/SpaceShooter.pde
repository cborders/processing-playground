SpaceShip ship;

void setup()
{
	size(800, 800);
	ship = new SpaceShip(width / 2, height / 2);
}

void draw()
{
	background(0);
	ship.update();
	ship.draw();
}

void keyPressed()
{
	if (key == ' ')
	{
		ship.boost(true);
	}

	if(keyCode == LEFT)
	{
		ship.rotateLeft(true);
	}

	if(keyCode == RIGHT)
	{
		ship.rotateRight(true);
	}
}

void keyReleased()
{
	if (key == ' ')
	{
		ship.boost(false);
	}

	if(keyCode == LEFT)
	{
		ship.rotateLeft(false);
	}

	if(keyCode == RIGHT)
	{
		ship.rotateRight(false);
	}
}
