int n = 0;
float c = 10;

void setup()
{
	size(800, 800);
	background(255);
	colorMode(HSB);
}

void draw()
{
	float angle = n * 137.5;
	float r = c * sqrt(n);

	int x = floor(r * cos(radians(angle))) + width / 2;
	int y = floor(r * sin(radians(angle))) + height / 2;

	float hue = map(r, 0, width / 2, 0, 255);

	fill(hue, 255, 255);
	stroke(0, 0, 128);
	strokeWeight(1);
	ellipse(x, y, 17, 17);

	n++;
}
